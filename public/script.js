jQuery(function ($) {
    const glyphs = {
        hiragana: {
            a: 'あ',
            i: 'い',
            u: 'う',
            e: 'え',
            o: 'お',
            ka: 'か',
            ki: 'き',
            ku: 'く',
            ke: 'け',
            ko: 'こ',
            sa: 'さ',
            si: 'し',
            su: 'す',
            se: 'せ',
            so: 'そ',
            ta: 'た',
            ti: 'ち',
            tu: 'つ',
            te: 'て',
            to: 'と',
            na: 'な',
            ni: 'に',
            nu: 'ぬ',
            ne: 'ね',
            no: 'の',
            ha: 'は',
            hi: 'ひ',
            hu: 'ふ',
            he: 'へ',
            ho: 'ほ',
            ma: 'ま',
            mi: 'み',
            mu: 'む',
            me: 'め',
            mo: 'も',
            ya: 'や',
            yu: 'ゆ',
            yo: 'よ',
            ra: 'ら',
            ri: 'り',
            ru: 'る',
            re: 'れ',
            ro: 'ろ',
            wa: 'わ',
            wo: 'を',
            n: 'ん'
        },
        katakana: {
            a: 'ア',
            i: 'イ',
            u: 'ウ',
            e: 'エ',
            o: 'オ',
            ka: 'カ',
            ki: 'キ',
            ku: 'ク',
            ke: 'ケ',
            ko: 'コ',
            sa: 'サ',
            si: 'シ',
            su: 'ス',
            se: 'セ',
            so: 'ソ',
            ta: 'タ',
            ti: 'チ',
            tu: 'ツ',
            te: 'テ',
            to: 'ト',
            na: 'ナ',
            ni: 'ニ',
            nu: 'ヌ',
            ne: 'ネ',
            no: 'ノ',
            ha: 'ハ',
            hi: 'ヒ',
            hu: 'フ',
            he: 'ヘ',
            ho: 'ホ',
            ma: 'マ',
            mi: 'ミ',
            mu: 'ム',
            me: 'メ',
            mo: 'モ',
            ya: 'ヤ',
            yu: 'ユ',
            yo: 'ヨ',
            ra: 'ラ',
            ri: 'リ',
            ru: 'ル',
            re: 'レ',
            ro: 'ロ',
            wa: 'ワ',
            wo: 'ヲ',
            n: 'ン'
        }
    };

    const container = $('#glyphs');

    $('input:radio').on('click', function () {
        const scriptName = $(this).val();
        const stats = $('<p/>');
        container.empty().append(
            $('<p/>').text(scriptName)
        ).append(stats);

        const scriptGlyphs = glyphs[scriptName];
        const enLettersShuffled = Object.keys(scriptGlyphs).sort(() => .5 - Math.random());
        const glyphCount = enLettersShuffled.length;
        stats.text('solved 0/' + glyphCount + ' (0%)');

        $.each(enLettersShuffled, function (unused, en) {
            const ja = scriptGlyphs[en];
            container.append(
                $('<span />').addClass('glyph ui-widget-content').attr({
                    id: en
                }).text(ja)
            );
        });

        $('.glyph').draggable({
            revert: 'invalid',
            snap: 'td.ui-widget-header',
            snapMode: 'inner',
            stop: function() {
                const solved = container.children('span').filter(function() {
                    return ['', '0px'].indexOf(this.style.top) === -1 &&
                            ['', '0px'].indexOf(this.style.left) === -1;
                }).length;
                stats.text('solved ' + solved + '/' + glyphCount + ' (' + Math.round(solved / glyphCount * 100) + '%)');
            }
        });
    });

    $('td').each(function () {
        const $this = $(this);
        const index = $this.index();
        const vowel = $this.siblings(':last').text();
        let consonant = $this.parents('tbody').siblings('thead').children().children().eq(index).text();

        let sound = consonant + vowel;

        if (index === 0) {
            if (vowel !== 'u') {
                return;
            }

            sound = 'n';
        }

        if (index === 10) {
            sound = vowel;
        }

        if (['yi', 'ye', 'wi', 'wu', 'we'].indexOf(sound) > -1) {
            return;
        }

        $this.attr('data-sound', sound);
    }).addClass('ui-widget-header').droppable({
        accept: function(el) {
            const $this = $(this);
            const targetSound = $this.data('sound');
            const glyphSound = el.attr('id');

            return targetSound === glyphSound;
        }
    });
});
